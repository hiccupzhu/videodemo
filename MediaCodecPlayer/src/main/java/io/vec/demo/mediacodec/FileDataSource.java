package io.vec.demo.mediacodec;

import android.annotation.TargetApi;
import android.media.MediaDataSource;
import android.os.Build;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by szhu on 2016/6/24.
 */
@TargetApi(Build.VERSION_CODES.M)
public class FileDataSource extends MediaDataSource {
    private String fileName;
    private RandomAccessFile localfile;

    public FileDataSource(String fileName){
        this.fileName = fileName;
        try {
            localfile = new RandomAccessFile(fileName, "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    @Override
    public int readAt(long position, byte[] buffer, int offset, int size) throws IOException {
        Log.i("info", String.format("read size=%d offset=%d position=%d", size, offset, position));
        if(position > localfile.length())
            return -1;
        localfile.seek(position);

        return localfile.read(buffer, 0, size);
    }

    @Override
    public long getSize() throws IOException {
        return localfile.length();
    }

    @Override
    public void close() throws IOException {
        localfile.close();
    }
}
