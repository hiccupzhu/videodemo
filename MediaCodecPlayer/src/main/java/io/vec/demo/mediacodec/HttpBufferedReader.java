package io.vec.demo.mediacodec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Arrays;

/**
 *
 */
public class HttpBufferedReader {
    private InputStream mInputStream;

    public HttpBufferedReader(InputStream instream){
        mInputStream = instream;
    }

    public static byte[] cutBytes(byte[] src, int start, int end){
        byte[] data = new byte[end-start+1];
        for(int i = start; i <= end; i ++){
            data[i-start] = src[i];
        }

        return data;
    }

    public byte[] readLine() {
        byte[] line = null;
        byte[] data = new byte[0x100000];
        try {
            byte b0, b1=0;
            int i;
            for(i = 0; i < data.length; i++) {
                byte b = (byte) mInputStream.read();
                b0 = b1;
                b1 = b;
                data[i] = b;
                if (b0 == '\r' && b1 == '\n') {
                    line = cutBytes(data, 0, i);
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return line;
    }


}
