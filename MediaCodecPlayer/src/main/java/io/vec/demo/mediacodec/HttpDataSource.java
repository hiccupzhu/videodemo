package io.vec.demo.mediacodec;

import android.annotation.TargetApi;
import android.media.MediaDataSource;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

//import java.io.BufferedReader;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Response;

import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by szhu on 2016/6/24.
 */
@TargetApi(Build.VERSION_CODES.M)
public class HttpDataSource extends MediaDataSource {
    private HttpReader reader;

    public HttpDataSource(String uri){
        reader = new HttpReader(uri);
        reader.start();
    }
    @Override
    public int readAt(long position, byte[] buffer, int offset, int size) throws IOException {
        Log.i("info", String.format("read size=%d offset=%d position=%d", size, offset, position));

        return reader.read(position, buffer, offset, size);
    }

    @Override
    public long getSize() throws IOException {
        return -1;
    }

    @Override
    public void close() throws IOException {
        reader.close();
    }



    class HttpReader extends Thread {
        private final OkHttpClient mOkHttpClient = new OkHttpClient();
        private Call mOkCall;
        private String mUri;
        private LinkedBlockingQueue<byte[]> mQueue;
//        private ByteBuffer mBuffer;
        private FileOutputStream fdump;
        MiniBuffer mBuffer;

        public HttpReader(String uri){
            mQueue = new LinkedBlockingQueue<byte[]>();
            mUri = uri;

            String filename = Environment.getExternalStorageDirectory() + "/dump.ts";
            Log.i("info", "Open file:"+filename);
            try {
                File file=new File(filename);
                if(!file.exists())
                    file.createNewFile();
                fdump = new FileOutputStream(filename);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

//            byte[] data = new byte[16*0x100000];
//            mBuffer = ByteBuffer.wrap(data);
            mBuffer = new MiniBuffer();
        }

        @Override
        public void run() {
            HttpBufferedReader in = null;
            try {
                URL realUrl = new URL(mUri);
                // 打开和URL之间的连接
                URLConnection connection = realUrl.openConnection();
                // 设置通用的请求属性
                connection.setRequestProperty("accept", "*/*");
                connection.setRequestProperty("connection", "Keep-Alive");
                connection.setRequestProperty("user-agent",
                        "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
                // 建立实际的连接
                connection.connect();
                // 获取所有响应头字段
                Map<String, List<String>> map = connection.getHeaderFields();
                // 遍历所有的响应头字段
                for (String key : map.keySet()) {
                    System.out.println(key + "--->" + map.get(key));
                }


                // 定义 BufferedReader输入流来读取URL的响应
                in = new HttpBufferedReader(connection.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            byte [] buffer;
            while(true){
                buffer = in.readLine();
                Log.i("info", "+read-length="+buffer.length);
//              line = line.trim();

//                try {
//                    fdump.write(buffer);
//                    fdump.flush();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

//                mQueue.add(buffer);
                mBuffer.wirte(buffer);
            }
        }

        public int read(long position, byte[] buffer, int offset, int size) {
            if(position == 0) {
                mBuffer.readSeek((int) position);
            }
            int bfsize = mBuffer.remaining();
            Log.i("info", String.format("++++++++++remained=%d size:%d", bfsize, size));
            while(size > bfsize){
//                byte[] data = null;
//                try {
//                    data = (byte[]) mQueue.take();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                Log.i("info", "-data-length="+data.length);
//                mBuffer.wirte(data);

                bfsize = mBuffer.remaining();
                try {
                    sleep(50);
                } catch (InterruptedException e) {
//                    e.printStackTrace();
                }
            }

            mBuffer.read(buffer, 0, size);

//            try {
//                fdump.write(buffer, 0, size);
//                fdump.flush();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            return size;
        }


        public int close(){
            mOkCall.cancel();

            return 0;
        }

    }


}
