package io.vec.demo.mediacodec;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;

import java.io.IOException;

import io.player.R;

public class MPHandlerActivity extends Activity {
	private MediaPlayer mPlayer = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		mPlayer = new MediaPlayer();
		setContentView(R.layout.player_activity);
		SurfaceView sv = (SurfaceView)findViewById(R.id.player_surface);
//		SurfaceView sv = new SurfaceView(this);
		sv.getHolder().addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mPlayer.setDisplay(null);
			}
		});
//		setContentView(sv);

//		mPlayer.setSurface(sv.getHolder().getSurface());

		Runnable r = new Runnable() {
			@Override
			public void run() {
				mPlayer.reset();
				HttpDataSource source = new HttpDataSource("http://36.110.146.10:20119/cctv5/encoder/0");
				mPlayer.setDataSource(source);
				try {
					mPlayer.prepare();
				} catch (IOException e) {
					e.printStackTrace();
				}
				mPlayer.start();
			}
		};

		new Handler().postDelayed(r, 5000);
//		new Thread(r).start();

	}

	protected void onDestroy() {
		super.onDestroy();
	}


}