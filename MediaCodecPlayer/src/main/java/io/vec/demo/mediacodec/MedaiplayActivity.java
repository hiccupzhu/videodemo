package io.vec.demo.mediacodec;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaFormat;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.nio.ByteBuffer;

public class MedaiplayActivity extends Activity implements SurfaceHolder.Callback {
	private static final String SAMPLE = Environment.getExternalStorageDirectory() + "/video.mp4";
	private PlayerThread mPlayerThread = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SurfaceView sv = new SurfaceView(this);
		sv.getHolder().addCallback(this);
		setContentView(sv);

	}

	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
		if (mPlayerThread == null) {
			mPlayerThread = new PlayerThread(holder.getSurface());
			mPlayerThread.start();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		if (mPlayerThread != null) {
			mPlayerThread.interrupt();
		}
	}


	private class PlayerThread extends Thread {
		private MediaPlayer mPlayer = null;
		private Surface surface;

		public PlayerThread(Surface surface) {
			this.surface = surface;
			mPlayer = new MediaPlayer();
		}

		@Override
		public void run() {
			HttpDataSource source = new HttpDataSource("http://36.110.146.10:20119/cctv5/encoder/0");
			mPlayer.setDataSource(source);
			mPlayer.setSurface(this.surface);
			try {
				mPlayer.prepare();
			} catch (IOException e) {
				e.printStackTrace();
			}
			mPlayer.start();
		}
	}
}