package io.vec.demo.mediacodec;

import android.util.Log;

import java.util.Objects;

/**
 * Created by szhu on 2016/6/29.
 */
public class MiniBuffer {
    protected byte[] mCache;
    private int mRindex = 0;
    private int mWindex = 0;
    private int end = 0;
    private Object lock = new Object();

    public MiniBuffer(){
        mCache = new byte[16*0x100000];

    }

    public int remaining(){
        int size = 0;
        if(mWindex >= mRindex){
            size = mWindex - mRindex;
        }else{
            size = mCache.length - mRindex;
            size += mWindex;
        }

        return size;
    }

    public int read(byte[] data, int offset, int size){
        int i = -1;
        synchronized (this.lock){
            int num = size < data.length ? size : data.length;
            for (i = 0; i < num; i++){
                data[i] = readByte();
            }
        }
        return i;
    }

    private byte readByte() {
        if(remaining() <= 0) return -1;

        byte b = mCache[mRindex++];
        if(mRindex >= mCache.length) mRindex = 0;

        return b;
    }

    public void readSeek(int pos) {
        Log.i("info", String.format("windx:%d rindex:%d seek-pos:%d", mWindex, mRindex, pos));
        if(mWindex >= mRindex){
            mRindex = pos;
        }else{
            mRindex += pos;
            if(mRindex > mCache.length){
                mRindex -= mCache.length;
            }
        }
    }

    public int wirte(byte[] data){
        int i = -1;
        synchronized (this.lock){
            for(i = 0; i < data.length; i++){
                wirteByte(data[i]);
            }
        }
        return i;
    }

    private void wirteByte(byte b){
        mCache[mWindex ++] = b;
        if(mWindex >= mCache.length){
            mWindex = 0;
        }
    }
}
