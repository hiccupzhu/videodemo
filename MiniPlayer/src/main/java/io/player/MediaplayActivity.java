package io.player;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.TimedMetaData;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.io.IOException;


public class MediaplayActivity extends Activity {
	private MediaPlayer mPlayer = null;
	private String mUri = null;
	private int mPlayIndex = 0;
//	private String[] mSamples;
	private TextView mTextView;
	private Handler mHandler;
	private StatusWorker mStatus;
	private SamplesWorker mSamplesWorker;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.player_activity);

		SurfaceView sv = null;
		sv = (SurfaceView)findViewById(R.id.player_surface);
//		sv = new SurfaceView(this);
		sv.getHolder().addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mPlayer.setDisplay(null);
			}
		});

		mTextView = (TextView)findViewById(R.id.msg_text);
		mTextView.setText("This is test code.");

		View root = (View)findViewById(R.id.root);
		root.setFocusable(true);
		root.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				Log.i("info", String.format("key-code:%d event:%s", keyCode, event.toString()));
				if (keyCode == KeyEvent.KEYCODE_DPAD_UP){
					if (event.getAction() == KeyEvent.ACTION_UP){
						playPre();
					}
					return true;
				}else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN){
					if (event.getAction() == KeyEvent.ACTION_UP){
						playNext();
					}
					return true;
				}else if (keyCode == KeyEvent.KEYCODE_BACK) {
					if (event.getAction() == KeyEvent.ACTION_UP && event.getRepeatCount() == 0){
						playerClose();
						System.exit(0);
					}
					return true;
				}else if (keyCode == KeyEvent.KEYCODE_DPAD_CENTER) {
					if (event.getAction() == KeyEvent.ACTION_UP){
						playPause();
					}
				}

				return false;
			}
		});

//

		mHandler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				if(msg.what == GlobelSetting.SET_DEBUG_VIEW){
					mTextView.setText(msg.getData().getString("msg"));
				}
			}
		};

		mStatus = new StatusWorker(mHandler);
		mStatus.start();

		mSamplesWorker = new SamplesWorker();
//		mSamplesWorker.start();

		this.playNext();
	}

	public void playPause(){
		if(mPlayer.isPlaying()){
			mPlayer.pause();
		}else{
			mPlayer.start();
		}
	}

	public void playNext(){
		String[] mSamples = mSamplesWorker.getmSamples();
		if(mPlayIndex >= mSamples.length || mPlayIndex < 0){
			mPlayIndex = 0;
		}
		this.playUri(mSamples[mPlayIndex++]);
	}

	public void playPre(){
		String[] mSamples = mSamplesWorker.getmSamples();
		if(mPlayIndex >= mSamples.length || mPlayIndex < 0){
			mPlayIndex = Math.max(mSamples.length - 1, 0);
		}
		this.playUri(mSamples[mPlayIndex--]);
	}

	public void playUri(String uri) {
		this.playerClose();
		mPlayer = new MediaPlayer();
		mStatus.setPlayer(mPlayer);
		mUri = uri;
		mPlayer.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
			@Override
			public void onBufferingUpdate(MediaPlayer mp, int percent) {
				Log.i("info", String.format("buffer:%d%", percent));
			}
		});

		mPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				Log.e("error", String.format("what:%d extra:%d", what, extra));
				return false;
			}
		});

		Runnable m = new Runnable() {
			@Override
			public void run() {

				try {
					mPlayer.setDataSource(mUri);
					mPlayer.prepare();
				} catch (IOException e) {
					e.printStackTrace();
				}
				mPlayer.start();

//				mTextView.setText("1233");
			}
		};

		/**
		 * Use Handler 方式创建：
		 * 注意：handler方式并没有创建新的线程，只是转让线程资源
		 */
		new Handler().postDelayed(m, 500);
		/**
		 * Use 线程的方式创建
		 */
//		new Thread(m).start();
	}


	public void playerClose() {
		if(mPlayer != null) {
			mPlayer.stop();
			mPlayer.release();
			mPlayer = null;
			mStatus.setPlayer(null);
		}

		mUri = null;
	}

}