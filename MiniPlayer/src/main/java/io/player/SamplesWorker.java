package io.player;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.internal.http.CacheStrategy;

import java.io.IOException;

/**
 * Created by szhu on 2016/7/1.
 */
public class SamplesWorker extends Thread {
    private Object lock = new Object();
    private String[] mSamples = null;

    public SamplesWorker(){
    }

    @Override
    public void run() {
        String lastBody = "";
        while(true) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder().url("http://www.hiccupzhu.com:8000/playlist").build();
            try {
                Response response = client.newCall(request).execute();
                if(response.isSuccessful()){
                    String body = response.body().string();
                    if(lastBody.equals(body) == false) {
                        Log.i("info", body);
                        this.setSamples(body.split("\n"));
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public void setSamples(String[] samples) {
        int count = 0;
        for(int i= 0; i < samples.length; i++, count++){
            if(samples[i].trim().equals("")) continue;
        }
        mSamples = new String[count];
        count = 0;
        for(int i= 0; i < samples.length; i++, count++){
            if(samples[i].trim().equals("")) continue;
            mSamples[count] = samples[i];
        }
    }

    public String[] getmSamples(){
        if(mSamples == null){
            return  GlobelSetting.samples;
        }else {
            return mSamples;
        }
    }

}
