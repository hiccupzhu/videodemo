package io.player;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

/**
 * Created by szhu on 2016/7/1.
 */
public class StatusWorker extends Thread {
    private MediaPlayer mPlayer = null;
    private Handler mHandler;
    private String mLastMessage = "";
    private Object lock = new Object();

    public StatusWorker(Handler handler){
        mPlayer = null;
        mHandler = handler;
    }

    @Override
    public void run() {
        while(true) {
            try {
                if (mPlayer != null && mPlayer.isPlaying()) {
                    String msg = String.format("%dx%d ",
                            mPlayer.getVideoWidth(),
                            mPlayer.getVideoHeight());
                    MediaPlayer.TrackInfo[] tInfos = mPlayer.getTrackInfo();
                    for (int i = 0; i < tInfos.length; i++) {
                        msg += String.format(" ", tInfos[i].toString());
                    }
                    if (mLastMessage.equals(msg) == false) {
                        this.sendString(msg);
                    }
                } else {
                    String msg = new String("No Play");
                    if (mLastMessage.equals(msg) == false) {
                        this.sendString(msg);
                    }
                }

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                String msg = e.toString();
                Log.e("error", "*****" + msg);
                if (mLastMessage.equals(msg) == false) {
                    this.sendString(msg);
                }
            }
        }
    }


    public void sendString(String msg) {
        Log.i("info", msg);
        mLastMessage = msg;

        Bundle b = new Bundle();
        b.putString("msg", msg);
        Message message = mHandler.obtainMessage();
        message.setData(b);
        message.what = GlobelSetting.SET_DEBUG_VIEW;
        mHandler.sendMessage(message);
    }


    public void setPlayer(MediaPlayer player){
        mPlayer = player;
    }
}
