package io.player;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

import io.vec.demo.mediacodec.R;

public class Mediaplay2Activity extends Activity {
	private MediaPlayer mPlayer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.player_activity);

		SurfaceView sv = null;
		sv = (SurfaceView)findViewById(R.id.player_surface);
//		sv = new SurfaceView(this);
		sv.getHolder().addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(SurfaceHolder holder) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
				mPlayer.setDisplay(holder);
			}

			@Override
			public void surfaceDestroyed(SurfaceHolder holder) {
				mPlayer.setDisplay(null);
			}
		});

		mPlayer = new MediaPlayer();

		Runnable m = new Runnable() {
			@Override
			public void run() {
				try {
					mPlayer.setDataSource("http://36.110.146.10:20119/zhejiang/encoder/0/playlist.m3u8");
					mPlayer.prepare();
				} catch (IOException e) {
					e.printStackTrace();
				}
				mPlayer.start();
			}
		};

		/**
		 * Use Handler 方式创建：
		 * 注意：handler方式并没有创建新的线程，只是转让线程资源
		 */
//		new Handler().postDelayed(m, 5000);
		/**
		 * Use 线程的方式创建
		 */
		new Thread(m).start();
	}

}